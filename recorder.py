#!/usr/bin/env python3
import requests
import sys
import time
import os
import threading
import datetime
import logging
import configparser

class StreamEnded(Exception):
	def __str__(self):
		return "StreamEnd Error"

class BreakLoop(Exception):
	pass


class Recorder(object):
	CONFIG = 'config'



	def __init__(self):
		self.avg_speed_MA = []
		self.output_dir = None
		self.temp_dir = None
		self.stream_url = None
		self.bit_rate = None
		self.read_length = None
		self.cmp_length = None
		self.proxy = None
		self.user_agent = None
		self.timeout = None
		self.check_for_loop = None
		self.num_reconnect_tries = None
		self.hole_buffer = None
		self.save_bandwith = None
		self.max_duration = None
		self.logger = logging.getLogger('Recorder')
		self.logger.setLevel(logging.INFO)
		self.handler = logging.FileHandler('log.txt')
		self.handler.setLevel(logging.INFO)
		self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.handler.setFormatter(self.formatter)
		self.logger.addHandler(self.handler)
		self.stdout = logging.StreamHandler(sys.stdout)
		self.stdout.setLevel(logging.DEBUG)
		self.stdout.setFormatter(self.formatter)
		self.logger.addHandler(self.stdout)
		self.read_config()
		self.setup()
		return


	def setup(self):
		# This function is used to setup the directory structure
		# and to setup the logging of the written files to allow
		# for the script to resart from a previous session


		self.check_old = True
		if not os.path.isdir(self.temp_dir):
			os.mkdir(self.temp_dir)
		now = datetime.datetime.now()
		self.f_out_dir =  self.temp_dir + '%i.%i.%i/' % (now.month,now.day,now.year)
		if not os.path.isdir(self.f_out_dir):
			os.mkdir(self.f_out_dir)
			self.check_old = False
		elif os.path.isfile(self.f_out_dir + 'mp3_info.log'):
			if len(open(self.f_out_dir + 'mp3_info.log','r').read()) == 0:
				self.check_old = False
		else:
			self.check_old = False

		self.mp3_logger = logging.getLogger('mp3 writer')
		self.mp3_logger.setLevel(logging.INFO)
		self.mp3_handler = logging.FileHandler(self.f_out_dir + 'mp3_info.log')
		self.mp3_logger.addHandler(self.mp3_handler)

	def read_config(self):
		# Reads in the info from the config and converts them to the necessary types
		config_parser = configparser.ConfigParser()
		config_parser.read('config')
		for key,value in config_parser['DEFAULT'].items():
			self.__dict__[key] = value
		self.byte_rate = int(self.bit_rate) // 8
		self.read_length = int(self.read_length)
		self.cmp_length = int(self.cmp_length)
		self.timeout = int(self.timeout)
		self.n_bytes = self.read_length*self.byte_rate
		self.check_for_loop = int(self.check_for_loop)
		if self.save_bandwith == 'True':
			self.save_bandwith = True
		else:
			self.save_bandwith = False
		if self.num_reconnect_tries == None:
			self.num_reconnect_tries = float('inf')
		self.num_reconnect_tries = int(self.num_reconnect_tries)
		self.hole_buffer = int(self.hole_buffer)
		if self.proxy:
			self.proxies = {'https':self.proxy}
		else:
			self.proxies = None
		if self.user_agent:
			self.headers = {'User-Agent':self.user_agent}
		else:
			self.headers = None

		if self.max_duration:
			self.max_duration = int(self.max_duration)
		



	def combine_mp3s(self,files, out):
		# Function to combine a list of mp3s in order.
		# The only need for mp3wrap and mp3splt is if
		# you want to preserve the metadata. Since this is
		# for streams, that doesn't need to be taken into
		# consideration.
		fout = open(out,'wb')

		for file in files:
			with open(file,'rb') as f:
				fout.write(f.read())
			f.close()
		fout.close()



	def get_stream_time(self):
		# This function will return the tuple of (hours,minutes)
		# for the time since the recording session began.
		hours = int((time.time() - self.start_time) / 3600)
		minutes = int((time.time() - self.start_time - 3600*hours) / 60)
		return (hours,minutes)



	def write_audio_to_file(self):


		# Async function to write the data from the stream to file.
		# This function is forked off as a child process of the script.
		# The dictionary 'write_info' is used to communicate between the
		# two processes in order to test if the stream has looped, or to 
		# test if the network cut out.
		try:
			write_info = self.write_info[self.n_files]
			
			write_info['kill'] = False
			out_stream = open(write_info['file'],write_info['mode'])
			in_stream = requests.get(self.stream_url,timeout=self.timeout,proxies=self.proxies,headers=self.headers,stream=True)
			write_info['start_time'] = time.time()
			t = time.time()
			for data in in_stream.iter_content(self.n_bytes):
				if write_info['kill']:
					sys.exit(1)
				dt = time.time() - t
				write_info['num_bytes'] += len(data)
				self.total_bytes_written += len(data)
				out_stream.write(data)
				out_stream.flush()
				if not write_info['beg']:
					write_info['beg'] = data
				elif len(write_info['beg']) < self.cmp_length*self.byte_rate:
					write_info['beg'] += data
				self.average_speed = write_info['num_bytes']/(time.time() - write_info['start_time'])
				if len(self.avg_speed_MA) < 5:
					self.avg_speed_MA.append(self.n_bytes/(time.time() - t))
				else:
					self.avg_speed_MA[-1] = self.n_bytes/(time.time() - t)
				t = time.time()
			raise StreamEnded

		except Exception as e:
			if write_info['log_error']:
				hours,minutes = self.get_stream_time()
				self.logger.info('Network Error: %s at time %ih:%im' % (e,hours,minutes))
			write_info['timeout'] = True
			write_info['end_time'] = time.time()
			out_stream.close()
			if write_info['num_bytes'] < self.cmp_length*self.byte_rate:
				write_info['start_new'] = True
				self.total_bytes_written -= write_info['num_bytes']
			else:
				f = open(write_info['file'],'rb')
				f.read(write_info['num_bytes'] - self.cmp_length*self.byte_rate)
				write_info['end'] = f.read()
				f.close()
			
			sys.exit(1)






	def check_fixed_gap(self, data):

		f = open(self.current_out,'rb').read()
		if f.find(data) != -1:
			return True

		else:
			return False





	def check_looped(self):
		# Check if the stream has looped around
		# using byte comparisons.
		f = open(self.current_out,'rb')
		data = f.read()
		if self.n_files == 1:
			data = data[self.cmp_length*self.byte_rate:]
		return data.find(self.start_of_stream)



	def reconnect(self,start=False,mode='wb'):
		# This function will attempt to reconnect to the stream
		# in the even there is a network error. It will also store 
		# the relevant times to help with filling gaps for subsequent loops.
		i = 0
		self.logger.info('Attempting to reconnect...')
		while i < self.num_reconnect_tries:
			self.current_write_thread = threading.Thread(target=self.write_audio_to_file)
			self.current_write_thread.setDaemon(True)
			self.write_info[self.n_files] = {
											'start_time':None,
											'timeout': False,
											'num_bytes':0,
											'file': self.current_out,
											'beg':None,
											'end':None,
											'start_new':False,
											'end_time':None,
											'log_error':False,
											'mode':mode
											}
			self.current_write_thread.start()
			time.sleep(self.timeout)
			if self.write_info[self.n_files]['timeout']:
				self.logger.info('Attempt %i failed' % (i+1))
				i += 1
				continue
			else:
				start_time = self.write_info[self.n_files]['start_time']
				try:
					while time.time() - start_time < self.cmp_length:
						if self.write_info[self.n_files]['timeout']:
							raise BreakLoop
				except:
					self.logger.info('Attempt %i failed' % (i+1))
					i += 1
					continue

				if start:
					self.start_time = time.time() - self.cmp_length - self.timeout
					self.start_of_stream = open(self.current_out,'rb').read(self.byte_rate*self.cmp_length)
				self.write_info[self.n_files]['log_error'] = True
				self.logger.info('Successfully Reconnected!')
				return

		self.logger.info('Exceeded Max Reconnect Tries')
		self.logger.info('Please Check Network Your Connection')
		self.logger.info('exiting...')
		sys.exit(1)





	def find_holes_save_bandwith(self):
		# After the loop is detected, calculate what times the recorder
		# needs to begin streaming again to get the missing parts.
		# This function will only reconnect to the stream when the 
		# time is close to the predicted missing gaps. 
		self.write_info[self.n_files]['kill'] = True
		if len(self.holes) == 0:
			return


		times = []
		files = []
		i = 1
		keys = list(self.write_info.keys())
		keys.sort()
		for a, hole in zip(keys,self.holes):
			parta = self.write_info[a]
			dt = parta['end_time'] - parta['start_time']
			if i == 1:
				start = self.ref_time + dt - self.hole_buffer - self.cmp_length
				end = start + hole['time'] + 2*self.hole_buffer + 2*self.cmp_length
				file = self.f_out_dir + 'hole_%i.mp3' % i
				hole['file'] = file
				files.append(file)
				times.append((start,end))
				self.ref_time += end - start + dt -2*self.hole_buffer -2*self.cmp_length
				i += 1
				continue

			prev_parta = self.write_info[ i - 1 ]
			if parta['start_time'] - prev_parta['end_time'] <= 0:
				prev_start,prev_end = times[i - 2]
				hole['file'] = files[-1]
				start = self.ref_time + dt - self.hole_buffer - self.cmp_length
				end = start + hole['time'] + 2*self.hole_buffer + 2*self.cmp_length
				times[i - 2] = (prev_start, end)
				self.ref_time -= prev_end - prev_start
				self.ref_time += end - prev_start
				continue

			else:
				start = self.ref_time + dt - self.hole_buffer - self.cmp_length
				end = start + hole['time'] + 2*self.hole_buffer + 2*self.cmp_length
				file = self.f_out_dir + 'hole_%i.mp3' % i
				hole['file'] = file
				files.append(file)
				times.append((start,end))
				self.ref_time += end - start + dt - 2*self.hole_buffer - 2*self.cmp_length
				i += 1


		max_time = times[-1][-1]
		hours = int((max_time - time.time()) / 3600)
		minutes = ((max_time - time.time() - hours*3600) / 60)
		self.logger.info("Total of %i gaps to fill" % len(files))
		self.logger.info("Estimated time until completion: %ih:%im" % (hours,minutes))
		i = 1
		for t,file in zip(times,files):
			start,end = t
			if (start - time.time())/60 > 0:
				self.logger.info('Waiting %.2f minutes to write gap of approximate length %.2fs' % ((start - time.time())/60,end - start))
				time.sleep(start - time.time())
			self.current_out = file
			self.n_files += 1
			self.write_info[self.n_files] = {
							'start_time':None,
							'timeout': False,
							'num_bytes':0,
							'file': self.current_out,
							'beg':None,
							'end':None,
							'start_new':False,
							'end_time':None,
							'log_error':True,
							'mode':'wb'
						}
			self.current_write_thread = threading.Thread(target=self.write_audio_to_file)
			self.current_write_thread.start()
			while time.time() <= end:
				time.sleep(1)
				if self.write_info[self.n_files]['timeout']:
					new_start = start + self.end_time - self.start_time
					new_end = new_start + end - start
					self.logger.info("Gap %i failed due to network error" % i)
					hours = int((new_end - time.time()) / 3600)
					minutes = ((new_end - time.time() - hours*3600) / 60)
					self.logger.info("Updated estimate of time until completion: %ih:%im" % (hours,minutes))
					times.append((new_start,new_end))
					files.append(file)
					break
			self.write_info[self.n_files]['kill'] = True
			i += 1





	def find_holes_dont_save_bandwith(self):
		# This function will continously write the subsequent loop(s)
		# to file until all of the gaps have been recorded.
		self.write_info[self.n_files]['kill'] = True
		if len(self.holes) == 0:
			return






		times = []
		i = 1
		file = self.f_out_dir + 'subsequent_loop.mp3'
		keys = list(self.write_info.keys())
		keys.sort()
		for a, hole in zip(keys,self.holes):
			parta = self.write_info[a]
			dt = parta['end_time'] - parta['start_time']
			if i == 1:
				start = self.ref_time + dt - self.hole_buffer - self.cmp_length
				end = start + hole['time'] + 2*(self.hole_buffer + self.cmp_length)
				hole['file'] = file
				times.append((start,end))
				self.ref_time += end - start + dt - 2*(self.hole_buffer + self.cmp_length)
				i += 1

			
			else:
				prev_parta = self.write_info[i]
				start = self.ref_time + dt - self.hole_buffer - self.cmp_length
				end = start + hole['time'] + 2*(self.hole_buffer + self.cmp_length)
				hole['file'] = file
				times.append((start,end))
				self.ref_time += end - start + dt - 2*(self.hole_buffer + self.cmp_length)
				i += 1


		max_time = times[-1][-1]
		hours = int((max_time - time.time()) / 3600)
		minutes = ((max_time - time.time() - hours*3600) / 60)
		self.logger.info("Total of %i gaps to fill" % len(self.holes))
		self.logger.info("Estimated time until completion: %ih:%im" % (hours,minutes))

		self.current_out = file
		self.n_files += 1
		self.write_info[self.n_files] = {
							'start_time':None,
							'timeout': False,
							'num_bytes':0,
							'file': self.current_out,
							'beg':None,
							'end':None,
							'start_new':False,
							'end_time':None,
							'log_error':True,
							'mode':'ab'
						}

		self.current_write_thread = threading.Thread(target=self.write_audio_to_file)
		self.current_write_thread.start()


		i = 0
		gaps_to_fill = list(range(len(self.holes)))
		gap_ind = gaps_to_fill[i]
		start_time,end_time = times[i]
		end_data = self.holes[gap_ind]['end']
		move_to_next_gap = False
		self.logger.info('Info for next nearest gap: starts in %.2fm, lasts for %.2fs' % ((start - time.time())/60,end - start))
		while i < len(gaps_to_fill):

			if move_to_next_gap:
				move_to_next_gap = False
				gap_ind = gaps_to_fill[i]
				start_time,end_time = times[gap_ind]
				end_data = self.holes[gap_ind]['end']
				self.logger.info('Info for next nearest gap: starts in %.2fm, lasts for %.2fs' % ((start - time.time())/60,end - start))



			if self.write_info[self.n_files]['timeout']:
				self.reconnect(mode='ab')

				if self.write_info[self.n_files]['start_time'] > start_time:
					self.logger.info("Capturing of gap %i failed because of network error" % gap_ind)
					gaps_to_fill.append(gap_ind)
					new_start = start_time + self.end_time - self.start_time
					new_end = new_start + end_time - start_time
					hours = int((new_end - time.time()) / 3600)
					minutes = ((new_end - time.time() - hours*3600) / 60)
					self.logger.info("Updated estimate of time until completion: %ih:%im" % (hours,minutes))
					times[gap_ind] = (new_start,new_end)
					move_to_next_gap = True
				continue


			if self.check_fixed_gap(end_data):
				move_to_next_gap = True
				i += 1
				continue

			time.sleep(self.read_length)
		self.write_info[self.n_files]['kill'] = True



















	def construct_output(self):
		# After the gaps have been recorded, this function will
		# recombine all the data so that a single full loop of the
		# stream will constructed.
		if not os.path.isdir(self.output_dir):
			os.mkdir(self.output_dir)
		now = datetime.datetime.now()
		temp_out = self.output_dir + '%i_%i_%i.mp3' % (now.month,now.day,now.year)
		fout = open(temp_out,'wb')
		for i in range(1,self.parts_end):
			parta = self.write_info[i]
			hole = self.holes[i - 1]
			partb = self.write_info[i + 1]

			fa = open(parta['file'],'rb').read()
			fhole = open(hole['file'],'rb').read()
			fb = open(partb['file'],'rb').read()

			a = fhole.find(parta['end'])
			b = fhole.find(partb['beg'])

			fout.write(fa)
			fout.write(fhole[a + self.byte_rate*self.cmp_length:b])
			fout.flush()


		if len(self.holes) == 0:
			f = open(self.write_info[self.n_files]['file'],'rb').read()
			fout.write(f[:self.loop_location[-1]])
			fout.flush()
			fout.close()

		else:
			fb = open(self.write_info[self.parts_end]['file'],'rb').read()
			fout.write(fb[:self.loop_location[-1]])
			fout.flush()
			fout.close()


		self.logger.info("Wrote full stream to file: %s" % temp_out)
		self.mp3_logger.info('COMPLETE!')
		return


	def start_from_previous(self):
		# If the the script detects that the stream
		# has been partially recorded on the same day,
		# this function will be called to attempt to restart
		# from where that session left off.
		self.write_info = {}
		self.holes = []
		self.n_files = 1
		self.found_loop = False
		self.fix_end_gap = False
		self.total_bytes_written = 0
		self.bytes_missing = 0

		f_log = open(self.f_out_dir + 'mp3_info.log','r').read()

		f_log = f_log.split('\n')
		for line in f_log[:]:
			if line == '':
				f_log.remove(line)

		if f_log[-1] == 'COMPLETE!':
			self.logger.info('Logs indicate stream has already been constructed for today')
			self.logger.info('If this is an error remove the %s folder and restart the script' % (self.f_out_dir))
			return

		if 'start gap' in f_log[-1]:
			del f_log[-1]
			f_logw = open(self.f_out_dir + 'mp3_info.log','w')
			f_logw.write('\n'.join(f_log))
			f_logw.close()

		if 'start part' in f_log[-1]:
			_,part,time_ = f_log[-1].split(' ')
			time_ = float(time_)
			gap = part.replace('part','gap')
			size = os.path.getsize(self.f_out_dir + part + '.mp3')
			length = size/self.byte_rate
			self.mp3_logger.info('end %s %f' % (part, time_ + length))
			self.mp3_logger.info('start %s %f' % (gap, time_ + length))
			c_t = time.time()
			self.mp3_logger.info('end %s %f' % (gap, c_t))
			f_log.append('end %s %f' % (part, time_ + length))
			f_log.append('start %s %f' % (gap, time_ + length))
			f_log.append('end %s %f' % (gap, c_t))
			self.end_gap = gap

		if 'end part' in f_log[-1]:
			_,part,time_ = f_log[-1].split(' ')
			time_ = float(time_)
			gap = part.replace('part','gap')
			size = os.path.getsize(self.f_out_dir + part + '.mp3')
			length = size/self.byte_rate
			self.mp3_logger.info('start %s %f' % (gap, time_))
			c_t = time.time()
			self.mp3_logger.info('end %s %f' % (gap, c_t))
			f_log.append('start %s %f' % (gap, time_))
			f_log.append('end %s %f' % (gap, c_t))
			self.end_gap = gap

		if 'filling gaps' in f_log[-1]:
			self.found_loop = True
			del f_log[-1]


		for start,end in zip(f_log[::2],f_log[1:][::2]):
			if 'part' in start:

				_,_,start_time = start.split(' ')
				_,part,end_time = end.split(' ')
				start_time = float(start_time)
				end_time = float(end_time)
				size = os.path.getsize(self.f_out_dir + part + '.mp3')
				self.total_bytes_written += size
				f_read = open(self.f_out_dir + part + '.mp3','rb').read()
				self.write_info[self.n_files] = {
						'start_time':start_time,
						'timeout': end_time,
						'num_bytes':size,
						'file': self.f_out_dir + part + '.mp3',
						'beg':f_read[:self.byte_rate*self.cmp_length],
						'end':f_read[-self.byte_rate*self.cmp_length:],
						'start_new':False,
						'end_time':end_time,
						'log_error':True,
						'mode':'wb'
					}
				if self.n_files == 1:
					self.start_of_stream = f_read[:self.byte_rate*self.cmp_length]



				self.n_files += 1


			else:

				_,_,start_time = start.split(' ')
				_,gap,end_time = end.split(' ')
				start_time = float(start_time)
				end_time = float(end_time)
				self.holes.append({'beg': self.n_files - 1,
										'end' : self.n_files,
										'missing': self.byte_rate*(end_time + start_time),
										'time': end_time - start_time,
										'file': None
									 })
				self.bytes_missing = self.byte_rate*(end_time + start_time)
		

		for i,hole in enumerate(self.holes):

			parta = self.write_info[hole['beg']]['end']
			hole['beg'] = parta

			if i != len(self.holes) - 1 or self.found_loop:
				partb = self.write_info[hole['end']]['beg']
				hole['end'] = partb

		if self.found_loop:
			self.n_files -= 1
			self.logger.info("Filling gaps from last run, don't believe time estimates")
			self.find_holes_dont_save_bandwith()
			self.construct_output()
			return



		else:

			last_session_start_time = self.write_info[1]['start_time']
			last_session_end_time = self.write_info[self.n_files - 1]['end_time']
			last_duration = last_session_end_time - last_session_start_time


			self.last_files = self.n_files - 1
			self.current_out = self.f_out_dir + 'part_%i.mp3' % self.n_files
			self.write_info[self.n_files] = {
							'start_time':None,
							'timeout': False,
							'num_bytes':0,
							'file': self.current_out,
							'beg':None,
							'end':None,
							'start_new':False,
							'end_time':None,
							'log_error':True,
							'mode':'wb'
						}
			self.start_time = time.time()
			self.current_write_thread = threading.Thread(target=self.write_audio_to_file)
			self.current_write_thread.setDaemon(True)
			self.current_write_thread.start()

			while (not self.write_info[self.n_files]['beg']) or len(self.write_info[self.n_files]['beg']) != self.cmp_length*self.byte_rate:
				if self.write_info[self.n_files]['timeout']:
					self.reconnect()
				time.sleep(1)


			self.logger.info('Successfully initialized, continuing from previous run')
			self.holes[-1]['end'] = self.write_info[self.n_files]['beg']
			self.start_time = self.write_info[self.n_files]['start_time']
			self.mp3_logger.info('start part_%i %f' % (self.n_files,self.start_time))
			while True:
				time.sleep(1)
				if self.write_info[self.n_files]['timeout']:

					if self.write_info[self.n_files]['start_new']:
							self.total_bytes_written -= self.write_info[self.n_files]['num_bytes']
							self.bytes_missing -= self.holes[-1]['missing']
							self.reconnect()
							dt = self.write_info[self.n_files]['start_time'] - self.write_info[self.n_files - 1]['end_time']
							self.holes[-1] = {'beg': self.write_info[self.n_files - 1]['end'],
											  'end'  : self.write_info[self.n_files]['beg'],
											  'missing': self.average_speed*dt,
											  'time': dt,
											  'file': None
											 }

							self.bytes_missing += self.average_speed*dt

					else:
						self.mp3_logger.info('end part_%i %f' % (self.n_files,self.write_info[self.n_files]['end_time']))
						self.mp3_logger.info('start gap_%i %f' % (self.n_files,self.write_info[self.n_files]['end_time']))
						self.n_files += 1
						self.current_out = self.f_out_dir + 'part_%i.mp3' % self.n_files
						self.reconnect()
						dt = self.write_info[self.n_files]['start_time'] - self.write_info[self.n_files - 1]['end_time']
						self.holes.append({'beg': self.write_info[self.n_files - 1]['end'],
											'end'  : self.write_info[self.n_files]['beg'],
											'missing': self.average_speed*dt,
											'time': dt,
											'file': None
										 })
						self.bytes_missing += self.average_speed*dt
					self.logger.info('Successfully reconnected, approximate number of bytes missing: %i approximate gap length: %.2fs' % (self.average_speed*dt, dt))
					self.mp3_logger.info('end gap_%i %f' % (self.n_files-1,self.write_info[self.n_files]['start_time']))
					self.mp3_logger.info('start part_%i %f' % (self.n_files,self.write_info[self.n_files]['start_time']))
					f = open(self.current_out,'rb')





				if self.found_loop:
					self.ref_time = time.time()
					self.end_time = self.ref_time
					self.parts_end = self.n_files
					self.mp3_logger.info('end part_%i %f' % (self.n_files,self.end_time))
					self.mp3_logger.info('filling gaps')

					this_session_time = self.end_time - self.start_time
					if self.max_duration:
						if self.end_time - last_session_start_time > self.max_duration:
							diff = self.max_duration - ((self.end_time - self.start_time) + (last_session_end_time - last_session_start_time))
							new_end_time = self.start_time - diff
							new_start_time = new_end_time - last_duration
							diff = new_start_time - last_session_start_time

							for i in range(1, last_files + 1):
								write_info[i]['start_time'] += diff
								write_info[i]['end_time'] += diff

							self.start_time = new_start_time


					else:

						self.logger.info('Warning! max_duration was not set so time predictions will not be accurate.')


					self.find_holes_dont_save_bandwith()
					self.construct_output()
					break


				if self.total_bytes_written + self.bytes_missing >= 0:
					ind = self.check_looped()
					if ind != -1:
						self.found_loop = True
						self.loop_location = (self.current_out, ind)
						self.logger.info("Found Loop in File: %s at %i" % self.loop_location)
						continue




















	def start_from_beggining(self):
		# If no previous recording session is detected, then this function
		# will be called.

		self.n_files = 1
		self.found_loop = False
		self.loop_location = None
		self.total_bytes_written = 0
		self.bytes_missing = 0
		self.write_info = {}
		self.holes = []
		self.stream_speed = []
		self.files = []
		self.average_speed = 0
		self.current_out = self.f_out_dir + 'part_%i.mp3' % self.n_files
		self.files.append(self.current_out)
		self.write_info[self.n_files] = {
							'start_time':None,
							'timeout': False,
							'num_bytes':0,
							'file': self.current_out,
							'beg':None,
							'end':None,
							'start_new':False,
							'end_time':None,
							'log_error':True,
							'mode':'wb'
						  }

		
		self.current_write_thread = threading.Thread(target=self.write_audio_to_file)
		self.current_write_thread.setDaemon(True)
		self.current_write_thread.start()
		self.start_time = time.time()
		while (not self.write_info[self.n_files]['beg']) or len(self.write_info[self.n_files]['beg']) != self.cmp_length*self.byte_rate:
			if self.write_info[self.n_files]['timeout']:
				break
			time.sleep(1)

		if not self.write_info[self.n_files]['timeout']:
			self.mp3_logger.info('start part_1 %f' % self.write_info[self.n_files]['start_time'])
		self.start_of_stream = self.write_info[self.n_files]['beg']
		self.logger.info('Successfully Initialized, recording has started.')
		while True:
			time.sleep(1)
			if self.write_info[self.n_files]['timeout']:

				if self.write_info[self.n_files]['start_new']:
					if self.n_files == 1:
						self.reconnect(start=True)
						self.logger.info('Successfully reconnected and session initialized to new starting point.')
						self.mp3_logger.info('start part_1 %f' % self.write_info[self.n_files]['start_time'])
						self.total_bytes_written = 0
						self.stream_speed = []
						f = open(self.current_out,'rb')
						continue
					else:
						self.total_bytes_written -= self.write_info[self.n_files]['num_bytes']
						self.bytes_missing -= self.holes[-1]['missing']
						self.reconnect()
						dt = self.write_info[self.n_files]['start_time'] - self.write_info[self.n_files - 1]['end_time']
						self.holes[-1] = {'beg': self.write_info[self.n_files - 1]['end'],
										  'end'  : self.write_info[self.n_files]['beg'],
										  'missing': self.average_speed*dt,
										  'time': dt,
										  'file': None
										 }

						self.bytes_missing += self.average_speed*dt

				else:
					self.mp3_logger.info('end part_%i %f' % (self.n_files,self.write_info[self.n_files]['end_time']))
					self.mp3_logger.info('start gap_%i %f' % (self.n_files,self.write_info[self.n_files]['end_time']))
					self.n_files += 1
					self.current_out = self.f_out_dir + 'part_%i.mp3' % self.n_files
					self.reconnect()
					dt = self.write_info[self.n_files]['start_time'] - self.write_info[self.n_files - 1]['end_time']
					self.holes.append({'beg': self.write_info[self.n_files - 1]['end'],
										'end'  : self.write_info[self.n_files]['beg'],
										'missing': self.average_speed*dt,
										'time': dt,
										'file': None
									 })
					self.bytes_missing += self.average_speed*dt
				self.logger.info('Successfully reconnected. Approximate bytes missing: %i, Approximate gap length: %.2fs' % (self.average_speed*dt, dt))
				self.mp3_logger.info('end gap_%i %f' % (self.n_files-1,self.write_info[self.n_files]['start_time']))
				self.mp3_logger.info('start part_%i %f' % (self.n_files,self.write_info[self.n_files]['start_time']))
				f = open(self.current_out,'rb')





			if self.found_loop:
				self.ref_time = time.time()
				self.end_time = self.ref_time
				self.parts_end = self.n_files
				self.mp3_logger.info('end part_%i %f' % (self.n_files,self.end_time))
				self.mp3_logger.info('filling gaps')
				if self.save_bandwith:
					self.find_holes_save_bandwith()
				else:
					self.find_holes_dont_save_bandwith()
				self.construct_output()
				break


			if self.total_bytes_written + self.bytes_missing >= self.check_for_loop*self.byte_rate:
				ind = self.check_looped()
				if ind != -1:
					self.found_loop = True
					self.loop_location = (self.current_out, ind)
					self.logger.info("Found Loop in File: %s at %i" % self.loop_location)
					continue











	def main(self):
		self.logger.info('Initializing Stream Recording Session.')
		if not self.check_old:
			self.start_from_beggining()
			return


		else:
			self.start_from_previous()
			return








if __name__ == '__main__':
	streamer = Recorder()
	streamer.main()














