# AudioStreamRecorder

This script was designed for an icecast mp3 stream that loops until a new stream is broadcasted. This script will write the stream to a file and complete when the stream has looped back to the starting point of the recording session.

List of features:

* Start recording from a previous, incomplete session.
* Will attempt to reconnect in case of network issues.
* Since network issues create gaps in the recording, the script has two modes to fill those gaps:
  * *save_bandwidth = true* mode will estimate when the gap data will occur and only connect to the stream when they are streamed.
  * *save_bandwidth = false* mode will continously check the stream until all the gaps have been filled.
* Logging with completion time estimates.

This script will generate an mp3 that is most likely not aligned to the begining of the stream. Please checkout [StreamAligner](https://github.com/RiemanZeta/StreamAligner/) for that functionality.


Please check the config file for the important parameters.
